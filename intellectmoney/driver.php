<?php

/**
 * @copyright 2000-2010 Amiro.CMS. All rights reserved.
 * @version $ Id: driver.php 54473 2010-10-13 13:20:53  Anton $
 * @package   Driver_PaymentSystem
 * @size 5934 xkqwnyykpxkwypllggnnqizkxnprlmznuypyqglyxlkyimgirwwmlqruxuqklmsinyxppnir
 * @since   5.8.6
 */
?>
<?php

/**
 * Example pay driver
 * 
 * How to create your own pay driver:
 * 
 * <ol>
 * <li>Copy the directory "_local/eshop/pay_drivers/example" to another, i.e. "_local/eshop/pay_drivers/my_driver"</li>
 * <li>Open driver.php in your path and:
 * <ul>
 * <li>Rename driver class name to your name (MyDriver_PaymentSystemDriver). Naming rule: all words delimeted by _ should be started from uppercase letter. Not _ allowed in driver name.</li>
 * <li>In some cases you should to check or add some fields when printing payment button on checkout page. You can manipulate with $aData array in getPayButton method for it. The same for pay system button with autoredirect but the method is getPayButtonParams</li>
 * <li>User will come back to the site from payment system by special URL and system already has the checks for setting correct order status. If you want to make your manual checking do it in payProcess method.</li>
 * <li>For payment system background requests for order approving there is payCallback method. You need to override this method with you own check of payment data.</li>
 * <li>If get or post field are differ from order_id, id or item_number you need to override getProcessOrder method that will return valid order id from POST and GET request.</li>
 * </ul>
 * </li>
 * <li>Open driver.tpl and modify sets:
 * <ul>
 * <li>settings_form - part of form that will be insertted to driver form when you open your driver for editing.</li>
 * <li>checkout_form - button that will be shown on checkout page after the list of items. ##hiddens## field is required.</li>
 * <li>pay_form - form that will be submitted to payment system. In most cases this form is made with autoredirect.</li>
 * <li>Also modify path to driver.lng.</li>
 * </ul>
 * </li>
 * <li>Captions for drivers you can set in driver.lng.</li>
 * <li>After all these steps install your driver in Settings/Pay drivers page of admin panel and edit parameters. Then include your diver for the payment in option "Allowed payment drivers" of Catalog : Orders setting.</li>
 * </ol>
 * 
 * @package Driver_PaymentSystem
 */
class Intellectmoney_PaymentSystemDriver extends AMI_PaymentSystemDriver {

    function ob_exit($status = null) {
	if ($status) {
	    ob_end_flush();
	    isset($_REQUEST['debug']) ? exit($status) : exit();
	} else {
	    ob_end_clean();
	    header("HTTP/1.0 200 OK");
	    echo "YES";
	    exit();
	}
    }

    function debug_file() {
	header('Content-type: text/plain; charset=utf-8');
	echo file_get_contents(__FILE__);
    }

    function format_amount(&$amount) {
	$amount = number_format($amount, 2, ".", "");
    }

    /**
     * Get checkout button HTML form
     *
     * @param array $aRes Will contain "error" (error description, 'Success by default') and "errno" (error code, 0 by default). "forms" will contain a created form
     * @param array $aData The data list for button generation
     * @param bool $bAutoRedirect If form autosubmit required (directly from checkout page)
     * @return bool true if form is generated, false otherwise
     */
    public function getPayButton(&$aRes, $aData, $bAutoRedirect = false) {

	$res = true;
	$aRes["error"] = "Success";
	$aRes["errno"] = 0;
	$data = $aData;

	if ($data['currency'] == 'RUR') {
	    $data['currency'] = 'RUB';
	}

	if (empty($data["process_url"])) {
	    $aRes["errno"] = 1;
	    $aRes["error"] = "process_url is missed";
	    $res = false;
	} else if (empty($data["im_eshopid"])) {
	    $aRes["errno"] = 2;
	    $aRes["error"] = "im_eshopid is missed";
	    $res = false;
	} else if (empty($data["amount"])) {
	    $aRes["errno"] = 4;
	    $aRes["error"] = "amount is missed";
	    $res = false;
	} else if (empty($data["button_name"]) && empty($data["button"])) {
	    $aRes["errno"] = 5;
	    $aRes["error"] = "button is missed";
	    $res = false;
	} else if ($data['currency'] != 'RUB') {
	    $aRes["errno"] = 6;
	    $aRes["error"] = "currency error";
	    $res = false;
	}

	$data["process_url"] = "";

	$this->format_amount($data["amount"]);
	foreach (Array("return", "cancel", "description", "button_name") as $fldName) {
	    $data[$fldName] = htmlspecialchars($data[$fldName]);
	}
	if (isset($data["process_url"]))
	    unset($data["process_url"]);
	if (isset($data["return"]))
	    unset($data["return"]);
	if (isset($data["callback"]))
	    unset($data["callback"]);
	if (isset($data["cancel"]))
	    unset($data["cancel"]);
	if (isset($data["amount"]))
	    unset($data["amount"]);
	if (isset($data["description"]))
	    unset($data["description"]);
	if (isset($data["button_name"]))
	    unset($data["button_name"]);
	if (isset($data["button"]))
	    unset($data["button"]);

	foreach ($data as $key => $value) {
	    $aData["hiddens"] .= "<input type=\"hidden\" name=\"$key\" value=\"$value\">\r\n";
	}

	if (!empty($aData["button"])) {
	    $aData["_button_html"] = 1;
	}
	if (!$res) {
	    $aData["disabled"] = "disabled";
	}

	return parent::getPayButton($aRes, $aData, $bAutoRedirect);
    }

    /**
     * Get the form that will be autosubmitted to payment system. This step is required for some shooping cart actions.
     *
     * @param array $aData The data list for button generation
     * @param array $aRes Will contain "error" (error description, 'Success by default') and "errno" (error code, 0 by default). "forms" will contain a created form
     * @return bool true if form is generated, false otherwise
     */
    public function getPayButtonParams($aData, &$aRes) {
	// Check parameters and set your fields here
	$data = $aData;
	$aRes["error"] = "Success";
	$aRes["errno"] = 0;

	$data['payment_url'] = "https://merchant.intellectmoney.ru/ru/";
	if ($data['currency'] == 'RUR') {
	    $data['currency'] = 'RUB';
	}

	$data['im_test_mode'] = !empty($data['im_test_mode']) ? 1 : 0;

	if ($data['hold_mode']) {
	    $data['hold_mode'] = 1;
	    $holdMode = 1;

	    $day = $data["hold_mode_date"];

	    if (intval($day) && $day > 0 && $day < 119) {
		$expireDate = date('Y-m-d H:i:s', strtotime('+' . $day . ' hours'));
	    } else {
		$expireDate = date('Y-m-d H:i:s', strtotime('+119 hours'));
	    }
	    $data['hold_mode_date'] = $expireDate;
	} else {
	    $data['hold_mode_date'] = '';
	}

	foreach (Array("payment_url", "return", "cancel") as $fldName) {
	    $data[$fldName] = htmlspecialchars($data[$fldName]);
	}

	$oEshopCart = AMI::getResource('eshop/cart');

	// Get cart total amount
	$aTotal = $oEshopCart->getTotal();

	// Get array of cart items objects
	$aCartItems = $oEshopCart->getItems();

	foreach ($aCartItems as $oCartItem) {
	    // Get cart item price info
	    $price = $oCartItem->getPriceInfo();
	    // Get cart item tax values
	    $oCartItem->getTax();
	    // Get cart item discount values
	    $oCartItem->getDiscount();
	    // Get cart item shipping values

	    $oCartItem->getItem();
	    $oCartItem->getQty();

	    $im_title = $oCartItem->getItem()->header;
	    $im_price = $price['order_price'];
	    $im_quant = $oCartItem->getQty();

	    $all_price += $im_price * $im_quant;

	    $positions[] = Array(
		'quantity' => $im_quant,
		//from IntellectMoney =>>>>
		'price' => $im_price,
		'tax' => $_REQUEST['nds'],
		'text' => $im_title,
	    );
	}

	if ($data['amount'] != $all_price) {
	    $im_delivery_price = $data['amount'] - $all_price;
	    $positions[] = Array(
		'quantity' => 1,
		'price' => $im_delivery_price,
		'tax' => $_REQUEST['nds'],
		'text' => 'Доставка',
	    );
	}

	$receipt = Array(
	    'inn' => $_REQUEST['im_inn'],
	    'group' => 'Main',
	    'content' => Array(
		'type' => 1,
		'customerContact' => $aData['email'],
		'positions' => $positions,
	    )
	);

	$merchantReceipt = json_encode($receipt);
	$data['merchantReceipt'] = $merchantReceipt;

	return parent::getPayButtonParams($data, $aRes);
    }

    /**
     * Verify the order from user back link. In success case 'accepted' status will be setup for order.
     *
     * @param array $aGet $_GET data
     * @param array $aPost $_POST data
     * @param array $aRes reserved array reference
     * @param array $aCheckData Data that provided in driver configuration
     * @return bool true if order is correct and false otherwise
     * @see AMI_PaymentSystemDriver::payProcess(...)
     */
    public function payProcess($aGet, $aPost, &$aRes, $aCheckData) {
	// See implplementation of this method in parent class

	return parent::payProcess($aGet, $aPost, $aRes, $aCheckData);
    }

    /**
     * Verify the order by payment system background responce. In success case 'confirmed' status will be setup for order.
     *
     * @param array $aGet $_GET data
     * @param array $aPost $_POST data
     * @param array $aRes reserved array reference
     * @param array $aCheckData Data that provided in driver configuration
     * @return int -1 - ignore post, 0 - reject(cancel) order, 1 - confirm order
     * @see AMI_PaymentSystemDriver::payCallback(...)
     */
    public function payCallback($aGet, $aPost, &$aRes, $aCheckData, array $aOrderData) {
	// See implplementation of this method in parent class
	ob_start();

	list($im_eshopid, $amount, $order, $im_test_mode, $lmi_sys_invs_no, $lmi_sys_trans_no, $payment_date, $secret_key, $payer_purse, $payer_wm, $lmi_prerequest, $epire_date, $im_cange_curr) = array($_REQUEST['LMI_PAYEE_PURSE'], number_format($_REQUEST['LMI_PAYMENT_AMOUNT'], 2, ".", ""), $_REQUEST['LMI_PAYMENT_NO'], $_REQUEST['LMI_SIM_MODE'], $_REQUEST['LMI_SYS_INVS_NO'], $_REQUEST['LMI_SYS_TRANS_NO'], $_REQUEST['LMI_SYS_TRANS_DATE'], $aCheckData['im_secretcode'], $_REQUEST['LMI_PAYER_PURSE'], $_REQUEST['LMI_PAYER_WM'], $_REQUEST['LMI_PREREQUEST'], $data['hold_mode_date'], $_REQUEST['UserField_1']);

	$infoPrice = number_format($aOrderData['total'], 2, ".", "");

	if ($amount != $infoPrice) {
	    $err = "ERROR: AMOUNT MISMATCH!\n";
	    $this->ob_exit($err);
	}

	//   AMOUNT and CURRENCY CODE CHECK

	if (!empty($aCheckData['im_test_mode'])) {
	    $data["im_test_mode"] = 1;
	    $data["recipientCurrency"] = 'TST';
	}

	if ($_REQUEST['LMI_MODE'] != $data['im_test_mode']) {
	    $err = "ERROR: CURRENCY MISMATCH!\n";
	    $this->ob_exit($err);
	}

	//   Проверка по секретному ключу
	if (!empty($_REQUEST['LMI_SECRET_KEY'])) {
	    //Проверка источника данных (по секретному ключу)
	    if ($aCheckData['im_secretcode'] != $_REQUEST['LMI_SECRET_KEY']) {
		$err = "ERROR: SECRET_KEY MISMATCH!\n";
		$this->ob_exit($err);
	    }
	    //Проверка номера сайта продавца
	    if ($im_eshopid != $aCheckData['im_eshopid']) {
		$err = "ERROR: INCORRECT ESHOP_ID!\n";
		$this->ob_exit($err);
	    }
	}

	// Проверка по контрольной подписи      
	else {

	    $sign = strtoupper(md5($_REQUEST["LMI_PAYEE_PURSE"] . $_REQUEST["LMI_PAYMENT_AMOUNT"] . $_REQUEST['item_number'] . $_REQUEST["LMI_MODE"] . $_REQUEST["LMI_SYS_INVS_NO"] . $_REQUEST["LMI_SYS_TRANS_NO"] . $_REQUEST["LMI_SYS_TRANS_DATE"] . $aCheckData['im_secretcode'] . $_REQUEST["LMI_PAYER_PURSE"] . $_REQUEST["LMI_PAYER_WM"]));

	    $sign_utf8 = iconv('windows-1251', 'utf-8', strtoupper(md5($params["LMI_PAYEE_PURSE"] . $params["LMI_PAYMENT_AMOUNT"] . $params['item_number'] . $params["LMI_MODE"] . $params["LMI_SYS_INVS_NO"] . $params["LMI_SYS_TRANS_NO"] . $params["LMI_SYS_TRANS_DATE"] . $aCheckData['im_secretcode'] . $params["LMI_PAYER_PURSE"] . $params["LMI_PAYER_WM"])));

	    if (($_REQUEST['LMI_HASH'] != $sign && $_REQUEST['LMI_HASH'] != $sign_utf8) || !$_REQUEST['LMI_HASH']) {

		$err = "ERROR: HASH MISMATCH\n";
		$this->ob_exit($err);
	    }
	}

	if ($_REQUEST['debug_file'])
	    $this->debug_file();
	$status = isset($aPost["LMI_SYS_TRANS_NO"]) ? 'fail' : 'inner';
	if (!@is_array($aGet))
	    $aGet = Array();
	if (!@is_array($aPost))
	    $aPost = Array();

	return ($status == 'inner' ? -1 : ($status == 'ok' ? 1 : 0));

	//return parent::payProcess($aGet, $aPost, $aRes, $aCheckData);
    }

    /**
     * Return real system order id from data that provided by payment system.
     *
     * @param array $aGet $_GET data
     * @param array $aPost $_POST data
     * @param array $aRes reserved array reference
     * @param array $aAdditionalParams reserved array
     * @return int order Id
     * @see AMI_PaymentSystemDriver::getProcessOrder(...)
     */
    public function getProcessOrder($aGet, $aPost, &$aRes, $aAdditionalParams) {

	$orderId = 0;
	if (!empty($aGet["LMI_PAYMENT_NO"]))
	    $orderId = $aGet["LMI_PAYMENT_NO"];
	if (!empty($aPost["LMI_PAYMENT_NO"]))
	    $orderId = $aPost["LMI_PAYMENT_NO"];
	return intval($orderId);
    }

    public function onPaymentConfirmed($orderId) {
	$this->ob_exit();
    }

}
