#Модуль оплаты платежной системы IntellectMoney для CMS Amiro

> **Внимание!** <br>
Данная версия актуальна на *25 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/display/TECH/Amiro+CMS#557747f575c95c87244baeafe14d987501fdd2.

Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/display/TECH/Amiro+CMS#557747b58dbc5833824bb0b845aaaa623c0c8d
